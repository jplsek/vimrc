People asked for my vimrc. So here it is. Keep in mind that there are some
quirks that I've dealt with and non-standard configurations.

Note that this is meant for use with Neovim 0.7+.

**WARNING: Running the installer WILL REPLACE YOUR FILES!**

The installer is expecting that this repo was cloned into `~/.vim`

### Requirements to run the installer
- python 3
- git

help.txt was my original notes for vim back in the day.

Please note that this repository contains configurations copied from other sources.

The history for this repo was reset upon making public.
