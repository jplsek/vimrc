#!/usr/bin/env python3
# This script is meant to be somewhat quick-and-dirty
import os
import sys
from argparse import ArgumentParser
from pathlib import Path
from shutil import rmtree
from subprocess import check_call

_ = check_call(['curl', '-fLo', os.path.expanduser('~/.vim/autoload/plug.vim'), '--create-dirs',
                'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'])

python = 'python3-'

if os.path.exists('/etc/debian_version'):
    _ = check_call(['sudo', 'apt-get', 'update'])
    install = ['sudo', 'apt-get', '-y', 'install']
elif os.path.exists('/etc/arch-release'):
    install = ['sudo', 'pacman', '-Sy', '--noconfirm']
    python = 'python-'
elif os.path.exists('/etc/fedora-release'):
    install = ['sudo', 'dnf', 'install', '-y']
elif sys.platform == 'darwin':
    install = ['brew', 'install']
else:
    print('Package manager not supported')
    sys.exit(1)

if sys.platform == 'linux' or sys.platform == 'linux2':
    packages = [python + 'neovim', 'nodejs', python + 'jedi', 'ctags', 'ripgrep', 'fd', 'wget']
    _ = check_call(install + packages)
elif sys.platform == 'darwin':
    packages = ['neovim', 'nodejs', 'ripgrep', 'fd', 'wget']
    _ = check_call(install + packages)
    _ = check_call(['python3', '-m', 'pip', 'install', '--user', '--upgrade', 'pynvim'])
else:
    print('OS not supported')
    sys.exit(1)

nvim = os.path.expanduser('~/.config/nvim')
if not os.path.lexists(nvim):
    if os.path.exists(nvim):
        rmtree(nvim)
    print('Creating nvim symlink')
    from_location = os.path.expanduser('~/.vim')
    os.symlink(from_location, nvim)

_ = check_call(['curl', '-o', 'js-debug.tar.gz', 'https://github.com/microsoft/vscode-js-debug/releases/download/v1.91.0/js-debug-dap-v1.91.0.tar.gz'])
_ = check_call(['tar', '-xzf', 'js-debug.tar.gz'])

print('Done!')
