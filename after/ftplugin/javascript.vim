setlocal tabstop=2
setlocal shiftwidth=2
setlocal softtabstop=2

setlocal omnifunc=javascriptcomplete#CompleteJS

setlocal inex=substitute(v:fname,'^\\~','./','')
