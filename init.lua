-- Bootstrap lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
    local lazyrepo = "https://github.com/folke/lazy.nvim.git"
    local out = vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
    if vim.v.shell_error ~= 0 then
        vim.api.nvim_echo({
            { "Failed to clone lazy.nvim:\n", "ErrorMsg" },
            { out,                            "WarningMsg" },
            { "\nPress any key to exit..." },
        }, true, {})
        vim.fn.getchar()
        os.exit(1)
    end
end
vim.opt.rtp:prepend(lazypath)

-- Make sure to setup `mapleader` and `maplocalleader` before
-- loading lazy.nvim so that mappings are correct.
-- This is also a good place to setup other settings (vim.opt)
vim.g.mapleader = " "
vim.g.maplocalleader = "\\"

-- ignore casing when searching
vim.opt.ignorecase = true
-- unless a capital letter is in the search
vim.opt.smartcase = true

-- 4 space tabs by default
vim.opt.expandtab = true
vim.opt.shiftwidth = 4
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.smartindent = true

-- show command while typing
vim.opt.showcmd = true

-- Don't draw the screen during macros
vim.opt.lazyredraw = true

vim.opt.wildignore:append({ "*/tmp/*", "*.so", "*.swp", "*.zip", "*.exe", "*.swp", ".git", "*.pyc", "*.tgz", "*.tar*" })

-- add characters for tabs
vim.opt.listchars = "tab:· "
vim.opt.list = true

vim.opt.number = true
vim.opt.cursorline = true

-- Set max and min cursor position before scrolling
vim.opt.scrolloff = 10

-- Statusline
vim.opt.laststatus = 2
vim.opt.statusline =
"%n %F %m %= %{get(b:,'gitsigns_status','')} %{get(b:,'gitsigns_head','')} %y %{(&fenc==\"\"?&enc:&fenc)} %{&ff} %-8.(%l,%c%)"

-- find command will search subdirectories
vim.opt.path:append({ "**" })

vim.opt.omnifunc = "syntaxcomplete#Complete"

-- add suffix to file search with gf
vim.opt.suffixesadd:append({ ".js", ".java", ".py", ".rb" })

-- update file if updated outside of session
vim.opt.autoread = true

-- leave buffer without needing to save
vim.opt.hidden = true

-- enable 24-bit color
vim.opt.termguicolors = true

-- enable persistent undo
vim.opt.undofile = true

-- immediate preview mode (like search and replace)
vim.opt.icm = "split"

-- smooth scroll instead of the cursor jumping around with wrapped text
vim.opt.smoothscroll = true

-- enable spell check since with treesitter, it just checks comments now (but it does make some stuff slower)
vim.opt.spell = true

-- enable loading other vim configs (project specific configs)
vim.o.exrc = true

-- remove tailing white space after moving
vim.g["Schlepp#trimWS"] = 0

-- disable netrw for custom tree
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

require("lazy").setup({
    -- make rename work to update packages and imports in file tree
    {
        "antosha417/nvim-lsp-file-operations",
        dependencies = {
            "nvim-lua/plenary.nvim",
            "nvim-tree/nvim-tree.lua",
        },
        opts = {}
    },

    -- smooth scrolling
    {
        'echasnovski/mini.animate',
        version = false,
        event = "VeryLazy",
        opts = function()
            -- https://github.com/LazyVim/LazyVim/blob/9ad1c49b67a5c4330e366cde41ab11b156de03f2/lua/lazyvim/plugins/extras/ui/mini-animate.lua#L8
            -- don't use animate when scrolling with the mouse
            local mouse_scrolled = false
            for _, scroll in ipairs({ "Up", "Down" }) do
                local key = "<ScrollWheel" .. scroll .. ">"
                vim.keymap.set({ "", "i" }, key, function()
                    mouse_scrolled = true
                    return key
                end, { expr = true })
            end

            local animate = require('mini.animate')
            return {
                cursor = {
                    enable = false
                },
                scroll = {
                    timing = animate.gen_timing.linear({ duration = 50, unit = 'total' }),
                    subscroll = animate.gen_subscroll.equal({
                        predicate = function(total_scroll)
                            if mouse_scrolled then
                                mouse_scrolled = false
                                return false
                            end
                            return total_scroll > 1
                        end,
                    }),
                },
                resize = {
                    enable = false
                }
            }
        end
    },

    -- reopen files at last edit position
    "farmergreg/vim-lastplace",

    -- various qol plugins
    {
        "folke/snacks.nvim",
        priority = 1000,
        lazy = false,
        opts = {
            -- handle large files by not loading as many plugins
            bigfile = {},

            -- show a dashboard when opening without a file
            dashboard = {
                sections = {
                    { icon = " ", title = "Keymaps", section = "keys", indent = 2, padding = 1 },
                    { icon = " ", title = "Recent Files", section = "recent_files", indent = 2, padding = 1 },
                    { icon = " ", title = "Projects", section = "projects", indent = 2, padding = 1 },
                    { section = "startup" },
                },
            },

            -- show indent lines
            indent = {
                animate = {
                    enabled = false
                },
            },

            -- show a modal for finding files or text
            picker = {
                layout = {
                    preset = "telescope"
                }
            },

            -- smooth scrolling
            -- However: I like the animation I have set up with mini.animate more
            -- scroll = {}

            -- nicer statuscolumn
            -- However: doesn't have mouse support for clicking on git hunks? throws errors when "No fold found"? so sticking with statuscol
            -- statuscolumn = {
            --     left = { "fold" },
            --     right = { "mark", "sign", "git" },
            --     folds = {
            --         open = true
            --     }
            -- },

            -- try to open files from cli asap (before loading plugins)
            quickfile = {}
        },
        keys = {
            -- Top Pickers & Explorer
            { "<leader><space>", function() require('snacks').picker.smart() end,                 desc = "Smart Find Files" },
            { "<leader>vb",      function() require('snacks').picker.buffers() end,               desc = "Buffers" },
            -- to search by file type, type file:$filetype, then ^g, then text to search, or vice-versa
            { "<leader>f",       function() require('snacks').picker.grep() end,                  desc = "Grep" },
            { "<leader>:",       function() require('snacks').picker.command_history() end,       desc = "Command History" },
            { "<leader>n",       function() require('snacks').picker.notifications() end,         desc = "Notification History" },
            { "<leader>e",       function() require('snacks').explorer() end,                     desc = "File Explorer" },
            -- find
            { "<leader>O",       function() require('snacks').picker.files() end,                 desc = "Find Files" },
            { "<leader>o",       function() require('snacks').picker.git_files() end,             desc = "Find Git Files" },
            { "<leader>rp",      function() require('snacks').picker.projects() end,              desc = "Projects" },
            { "<leader>re",      function() require('snacks').picker.recent() end,                desc = "Recent" },
            -- git
            { "<leader>gb",      function() require('snacks').picker.git_branches() end,          desc = "Git Branches" },
            { "<leader>gl",      function() require('snacks').picker.git_log() end,               desc = "Git Log" },
            { "<leader>gL",      function() require('snacks').picker.git_log_line() end,          desc = "Git Log Line" },
            { "<leader>gs",      function() require('snacks').picker.git_status() end,            desc = "Git Status" },
            { "<leader>gS",      function() require('snacks').picker.git_stash() end,             desc = "Git Stash" },
            { "<leader>gd",      function() require('snacks').picker.git_diff() end,              desc = "Git Diff (Hunks)" },
            { "<leader>gf",      function() require('snacks').picker.git_log_file() end,          desc = "Git Log File" },
            { "<leader>gg",      function() require('snacks').picker.git_grep() end,              desc = "Git Grep" },
            -- Grep
            { "<leader>sb",      function() require('snacks').picker.lines() end,                 desc = "Buffer Lines" },
            { "<leader>sB",      function() require('snacks').picker.grep_buffers() end,          desc = "Grep Open Buffers" },
            { "<leader>F",       function() require('snacks').picker.grep_word() end,             desc = "Visual selection or word", mode = { "n", "x" } },
            -- search
            { '<leader>s"',      function() require('snacks').picker.registers() end,             desc = "Registers" },
            { '<leader>s/',      function() require('snacks').picker.search_history() end,        desc = "Search History" },
            { "<leader>sa",      function() require('snacks').picker.autocmds() end,              desc = "Autocmds" },
            { "<leader>sb",      function() require('snacks').picker.lines() end,                 desc = "Buffer Lines" },
            { "<leader>sc",      function() require('snacks').picker.command_history() end,       desc = "Command History" },
            { "<leader>sC",      function() require('snacks').picker.commands() end,              desc = "Commands" },
            { "<leader>sd",      function() require('snacks').picker.diagnostics() end,           desc = "Diagnostics" },
            { "<leader>sD",      function() require('snacks').picker.diagnostics_buffer() end,    desc = "Buffer Diagnostics" },
            { "<leader>sh",      function() require('snacks').picker.help() end,                  desc = "Help Pages" },
            { "<leader>sH",      function() require('snacks').picker.highlights() end,            desc = "Highlights" },
            { "<leader>si",      function() require('snacks').picker.icons() end,                 desc = "Icons" },
            { "<leader>sj",      function() require('snacks').picker.jumps() end,                 desc = "Jumps" },
            { "<leader>sk",      function() require('snacks').picker.keymaps() end,               desc = "Keymaps" },
            { "<leader>sl",      function() require('snacks').picker.loclist() end,               desc = "Location List" },
            { "<leader>sm",      function() require('snacks').picker.marks() end,                 desc = "Marks" },
            { "<leader>sM",      function() require('snacks').picker.man() end,                   desc = "Man Pages" },
            { "<leader>sp",      function() require('snacks').picker.lazy() end,                  desc = "Search for Plugin Spec" },
            { "<leader>sq",      function() require('snacks').picker.qflist() end,                desc = "Quickfix List" },
            { "<leader>rr",      function() require('snacks').picker.resume() end,                desc = "Resume" },
            { "<leader>su",      function() require('snacks').picker.undo() end,                  desc = "Undo History" },
            -- LSP
            { "gd",              function() require('snacks').picker.lsp_definitions() end,       desc = "Goto Definition" },
            { "gD",              function() require('snacks').picker.lsp_declarations() end,      desc = "Goto Declaration" },
            { "<leader>lr",      function() require('snacks').picker.lsp_references() end,        nowait = true,                     desc = "References" },
            { "<leader>li",      function() require('snacks').picker.lsp_implementations() end,   desc = "Goto Implementation" },
            { "gy",              function() require('snacks').picker.lsp_type_definitions() end,  desc = "Goto T[y]pe Definition" },
            { "<leader>ss",      function() require('snacks').picker.lsp_symbols() end,           desc = "LSP Symbols" },
            { "<leader>sS",      function() require('snacks').picker.lsp_workspace_symbols() end, desc = "LSP Workspace Symbols" },
            { '<leader><CR>',    function() vim.lsp.buf.code_action() end,                        mode = { "n", "v" }, },
        },
    },

    -- quickfix/diagnostics/definitions/problems in a window pane
    {
        "folke/trouble.nvim",
        opts = {}, -- for default options, refer to the configuration section for custom setup.
        cmd = "Trouble",
        keys = {
            {
                "<leader>xx",
                "<cmd>Trouble diagnostics toggle<cr>",
                desc = "Diagnostics (Trouble)",
            },
            {
                "<leader>xX",
                "<cmd>Trouble diagnostics toggle filter.buf=0<cr>",
                desc = "Buffer Diagnostics (Trouble)",
            },
            {
                "<leader>xs",
                "<cmd>Trouble symbols toggle focus=false<cr>",
                desc = "Symbols (Trouble)",
            },
            {
                "<leader>xl",
                "<cmd>Trouble lsp toggle focus=false win.position=right<cr>",
                desc = "LSP Definitions / references / ... (Trouble)",
            },
            {
                "<leader>xL",
                "<cmd>Trouble loclist toggle<cr>",
                desc = "Location List (Trouble)",
            },
            {
                "<leader>xQ",
                "<cmd>Trouble qflist toggle<cr>",
                desc = "Quickfix List (Trouble)",
            },
        },
    },

    -- show available keybindings in a popup - also replaces certain menus, like spell suggestions
    {
        "folke/which-key.nvim",
        event = "VeryLazy",
        opts = {
            preset = 'helix',
            delay = 500,
            triggers = {
                { "<auto>",   mode = "nixsotc" },
                { "<leader>", mode = { "n", "v" } },
            }
        },
        keys = {
            {
                "<leader>?",
                function()
                    require("which-key").show({ global = false })
                end,
                desc = "Buffer Local Keymaps (which-key)",
            },
        },
    },

    -- rainbow curly braces, etc
    {
        "HiPhish/rainbow-delimiters.nvim",
        config = function()
            require('rainbow-delimiters.setup').setup {
                query = {
                    tsx = 'rainbow-parens'
                }
            }
        end
    },

    -- autocomplete
    {
        "hrsh7th/nvim-cmp",
        dependencies = {
            "hrsh7th/cmp-nvim-lsp",
            "hrsh7th/cmp-buffer",
            "hrsh7th/cmp-path",
            "hrsh7th/cmp-cmdline",
            "hrsh7th/cmp-vsnip",
            "hrsh7th/vim-vsnip",
        },
        config = function()
            local kind_icons = {
                Text = "",
                Method = "󰆧",
                Function = "󰊕",
                Constructor = "",
                Field = "󰇽",
                Variable = "󰂡",
                Class = "󰠱",
                Interface = "",
                Module = "",
                Property = "󰜢",
                Unit = "",
                Value = "󰎠",
                Enum = "",
                Keyword = "󰌋",
                Snippet = "",
                Color = "󰏘",
                File = "󰈙",
                Reference = "",
                Folder = "󰉋",
                EnumMember = "",
                Constant = "󰏿",
                Struct = "",
                Event = "",
                Operator = "󰆕",
                TypeParameter = "󰅲",
            }

            local cmp = require 'cmp'

            cmp.setup({
                formatting = {
                    format = function(entry, vim_item)
                        vim_item.kind = string.format('%s %s', kind_icons[vim_item.kind], vim_item.kind)
                        return vim_item
                    end
                },
                snippet = {
                    expand = function(args)
                        vim.fn["vsnip#anonymous"](args.body)
                    end,
                },
                mapping = cmp.mapping.preset.insert({
                    ['<C-b>'] = cmp.mapping.scroll_docs(-4),
                    ['<C-f>'] = cmp.mapping.scroll_docs(4),
                    ['<C-Space>'] = cmp.mapping.complete(),
                    ['<C-e>'] = cmp.mapping.abort(),
                    ['<CR>'] = cmp.mapping.confirm({ select = true }),
                    ['<Tab>'] = cmp.mapping.confirm({ select = true }),
                }),
                sources = cmp.config.sources({
                    { name = 'nvim_lsp' },
                    { name = 'vsnip' },
                }, {
                    { name = 'buffer' },
                })
            })

            cmp.setup.filetype('gitcommit', {
                sources = cmp.config.sources({
                    { name = 'buffer' },
                })
            })

            cmp.setup.cmdline({ '/', '?' }, {
                mapping = cmp.mapping.preset.cmdline(),
                sources = {
                    { name = 'buffer' }
                }
            })

            cmp.setup.cmdline(':', {
                mapping = cmp.mapping.preset.cmdline(),
                sources = cmp.config.sources({
                    { name = 'path' }
                }, {
                    { name = 'cmdline' }
                })
            })
        end
    },

    -- automatic installation of mason dap debuggers
    "jay-babu/mason-nvim-dap.nvim",

    -- clears highlighed search and better search
    "junegunn/vim-slash",

    -- show lsp status
    {
        "j-hui/fidget.nvim",
        opts = {
            notification = {
                override_vim_notify = true,
                window = {
                    winblend = 0, -- Background color opacity in the notification window
                },
            },
        },
    },

    -- required for textobj plugins
    "kana/vim-textobj-user",

    -- modern folds
    {
        'kevinhwang91/nvim-ufo',
        dependencies = {
            'kevinhwang91/promise-async',
        },
        config = function()
            vim.o.foldcolumn = '1' -- '0' is not bad
            vim.o.foldlevel = 99   -- Using ufo provider need a large value, feel free to decrease the value
            vim.o.foldlevelstart = 99
            vim.o.foldenable = true
            vim.o.fillchars = [[eob: ,fold: ,foldopen:,foldsep: ,foldclose:]]

            local ufo = require('ufo')

            ufo.setup({
                close_fold_kinds_for_ft = {
                    default = { 'imports' },
                },
            })

            -- Using ufo provider need remap `zR` and `zM`
            vim.keymap.set('n', 'zR', ufo.openAllFolds)
            vim.keymap.set('n', 'zM', ufo.closeAllFolds)
        end
    },

    -- git gutter
    {
        "lewis6991/gitsigns.nvim",
        opts = {},
        keys = {
            { '<leader>hr', function() require('gitsigns').reset_hunk() end,   desc = "Reset hunk" },
            { '<leader>hp', function() require('gitsigns').preview_hunk() end, desc = "Preview hunk" }
        }
    },

    -- nicer status column
    {
        "luukvbaal/statuscol.nvim",
        opts = function()
            local builtin = require("statuscol.builtin")
            return {
                relculright = true,
                segments = {
                    -- folds (click = folds)
                    { text = { builtin.foldfunc },      click = "v:lua.ScFa" },
                    -- diagnostic (click = show diagnostic) - but they show on the git sign anyways
                    -- {sign = { namespace = { "diagnostic/signs" } }, click = "v:lua.ScSa"},
                    -- other stuff not set - like breakpoints - but they show on the git sign anyways
                    -- {sign = { name = { ".*" }, colwidth = 1, wrap = true }, click = "v:lua.ScSa"},
                    -- line numbers (click = add breakpoint)
                    { text = { builtin.lnumfunc, " " }, click = "v:lua.ScLa" },
                    -- git (click = show diff)
                    { text = { "%s" },                  click = "v:lua.ScSa" },
                }
            }
        end,
    },

    -- undotree/history viewer
    {
        "mbbill/undotree",
        keys = {
            {
                "<leader>u",
                ":UndotreeToggle<cr>",
                silent = true,
                mode = { "n" },
                desc = "Toggle undotree",
            }
        },
        lazy = true,
    },

    -- debugger
    {
        "mfussenegger/nvim-dap",
        keys = {
            {
                '<F5>',
                function() require('dap').continue() end,
                mode = { 'n' },
                desc = "Debugger: continue",
            },
            {
                '<F10>',
                function() require('dap').step_over() end,
                mode = { 'n' },
                desc = "Debugger: step over",
            },
            {
                '<F11>',
                function() require('dap').step_into() end,
                mode = { 'n' },
                desc = "Debugger: step into",
            },
            {
                '<F12>',
                function() require('dap').step_out() end,
                mode = { 'n' },
                desc = "Debugger: step out",
            },
            {
                '<Leader>b',
                function() require('dap').toggle_breakpoint() end,
                mode = { 'n' },
                desc = "Toggle breakpoint",
            },
        }
    },

    -- multiple cursors
    "mg979/vim-visual-multi",

    -- theme
    "Mofiqul/vscode.nvim",

    -- rust - for neotest and dap adapters - need to run: rustup component add rust-analyzer
    {
        'mrcjkb/rustaceanvim',
        version = '^4',
        init = function()
            vim.g.rustaceanvim = {
                -- LSP configuration
                server = {
                },
            }
        end
    },

    -- lsp
    "neovim/nvim-lspconfig",

    -- java
    { 'nvim-java/nvim-java' },

    -- testing
    {
        "nvim-neotest/neotest",
        dependencies = {
            "nvim-neotest/nvim-nio",
            "nvim-lua/plenary.nvim",
            "antoinemadec/FixCursorHold.nvim",
            "nvim-treesitter/nvim-treesitter",
            'nvim-neotest/neotest-jest',
            'rcasia/neotest-java', -- run :NeotestJava setup
        },
        opts = function()
            return {
                adapters = {
                    require('rustaceanvim.neotest'),
                    require('neotest-jest')({
                        jestCommand = "npm test --",
                        jestConfigFile = "jest.config.ts",
                        env = { CI = true },
                        cwd = function(path)
                            return vim.fn.getcwd()
                        end,
                    }),
                    require("neotest-java")({
                        ignore_wrapper = false, -- whether to ignore maven/gradle wrapper
                        -- junit_jar = nil, -- default: ~/.local/share/nvim/neotest-java/junit-platform-console-standalone-[version].jar
                    })
                },
            }
        end,
        keys = {
            {
                '<leader>tt',
                function()
                    vim.notify("Running test...")
                    require("neotest").run.run()
                end,
                mode = { "n" },
                desc = "Run test",
            },
            {
                '<leader>tT',
                function()
                    require("neotest").run.run({ strategy = "dap" })
                end,
                mode = { "n" },
                desc = "Run test in debug mode",
            },
            {
                '<leader>tf',
                function()
                    vim.notify("Running test file...")
                    require("neotest").run.run(vim.fn.expand("%"))
                end,
                mode = { "n" },
                desc = "Run test file",
            },
            {
                '<leader>to',
                function()
                    require("neotest").output_panel.open()
                end,
                mode = { "n" },
                desc = "Open test output panel",
            },
        },
        lazy = true
    },

    -- tree
    {
        "nvim-tree/nvim-tree.lua",
        config = function()
            -- https://github.com/nvim-tree/nvim-tree.lua/wiki/Recipes#sorting-files-naturally-respecting-numbers-within-files-names
            local function natural_sort(left, right)
                left = left.name:lower()
                right = right.name:lower()

                if left == right then
                    return false
                end

                for i = 1, math.max(string.len(left), string.len(right)), 1 do
                    local l = string.sub(left, i, -1)
                    local r = string.sub(right, i, -1)

                    if type(tonumber(string.sub(l, 1, 1))) == "number" and type(tonumber(string.sub(r, 1, 1))) == "number" then
                        local l_number = tonumber(string.match(l, "^[0-9]+"))
                        local r_number = tonumber(string.match(r, "^[0-9]+"))

                        if l_number ~= r_number then
                            return l_number < r_number
                        end
                    elseif string.sub(l, 1, 1) ~= string.sub(r, 1, 1) then
                        return l < r
                    end
                end

                return false
            end

            require("nvim-tree").setup({
                actions = {
                    open_file = {
                        resize_window = false,
                    }
                },
                sort_by = function(nodes)
                    table.sort(nodes, natural_sort)
                end
            })

            -- https://github.com/nvim-tree/nvim-tree.lua/wiki/Recipes#automatically-open-file-upon-creation
            local api = require("nvim-tree.api")
            api.events.subscribe(api.events.Event.FileCreated, function(file)
                vim.cmd("edit " .. file.fname)
            end)

            -- https://github.com/nvim-tree/nvim-tree.lua/wiki/Recipes#remember-nvimtree-window-size
            local view = require('nvim-tree.view')

            -- save window width on WinResized event
            vim.api.nvim_create_augroup('save_nvim_tree_width', { clear = true })
            vim.api.nvim_create_autocmd('WinResized', {
                group = 'save_nvim_tree_width',
                pattern = '*',
                callback = function()
                    local filetree_winnr = view.get_winnr()
                    if filetree_winnr ~= nil and vim.tbl_contains(vim.v.event['windows'], filetree_winnr) then
                        vim.t['filetree_width'] = vim.api.nvim_win_get_width(filetree_winnr)
                    end
                end,
            })

            -- restore window size when opening
            api.events.subscribe(api.events.Event.TreeOpen, function()
                if vim.t['filetree_width'] ~= nil then
                    view.resize(vim.t['filetree_width'])
                end
            end)
        end,
        keys = {
            {
                "<leader><tab>",
                ":NvimTreeFindFile<cr>",
                silent = true,
                mode = { "n" },
                desc = "Open tree folder panel",
            }
        }
    },

    -- icons
    'nvim-tree/nvim-web-devicons',

    -- use treesitter for syntax parsing, etc
    {
        "nvim-treesitter/nvim-treesitter",
        dependencies = {
            {
                'nvim-treesitter/nvim-treesitter-textobjects'
            }
        },
        config = function()
            require 'nvim-treesitter.configs'.setup {
                -- Automatically install missing parsers when entering buffer
                auto_install = true,

                highlight = {
                    enable = true,

                    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
                    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
                    -- Using this option may slow down your editor, and you may see some duplicate highlights.
                    -- Instead of true it can also be a list of languages
                    additional_vim_regex_highlighting = false,
                },

                textobjects = {
                    swap = {
                        enable = true,
                        swap_next = {
                            ["]g"] = "@parameter.inner",
                        },
                        swap_previous = {
                            ["[g"] = "@parameter.inner",
                        },
                    },
                    move = {
                        enable = true,
                        goto_next_start = {
                            [']h'] = '@parameter.inner',
                        },
                        goto_previous_start = {
                            ['[h'] = '@parameter.inner',
                        },
                    },
                },

                -- :h nvim-treesitter-incremental-selection-mod
                incremental_selection = {
                    enable = true,
                    keymaps = {
                        init_selection = "<C-.>",     -- in normal mode, start incremental selection
                        node_incremental = "<C-\\>",  -- in visual mode, increment to the upper named parent
                        node_decremental = "<C-|>",   -- in visual mode, decrement to the previous named node
                        scope_incremental = "<C-\">", -- in visual mode, increment to the upper scope (as defined in `locals.scm`)
                    },
                }
            }
        end,
        build = ":TSUpdate",
    },

    -- show context on top of screen
    {
        "nvim-treesitter/nvim-treesitter-context",
        opts = {
            max_lines = 5,
            min_window_height = 20,
            mode = 'topline'
        }
    },

    -- dap ui
    {
        "rcarriga/nvim-dap-ui",
        dependencies = {
            "mfussenegger/nvim-dap",
            "nvim-neotest/nvim-nio",
        },
        opts = {},
        keys = {
            {
                '<Leader>at',
                function() require('dapui').toggle() end,
                mode = { 'n' },
                desc = "Toggle debugger UI",
            }
        }
    },

    -- jump between alternate files, like between tests and implementations
    {
        "rgroli/other.nvim",
        config = function()
            require("other-nvim").setup({
                mappings = {
                    {
                        pattern = "(.*)/src/main/java/(.*).java$",
                        target = "%1/src/test/java/%2Test.java",
                        context = "test"
                    },
                    {
                        pattern = "(.*)/src/test/java/(.*)Test.java$",
                        target = "%1/src/main/java/%2.java",
                        context = "source"
                    },
                    {
                        pattern = "(.*)/([a-zA-Z0-9]*).ts$", -- don't include `"."spec`
                        target = "%1/__tests__/%2.spec.ts",
                        context = "test"
                    },
                    {
                        pattern = "(.*)/__tests__/(.*).spec.ts$",
                        target = "%1/%2.ts",
                        context = "source"
                    },
                    {
                        pattern = "(.*)/([a-zA-Z0-9]*).tsx$", -- don't include `"."spec`
                        target = "%1/__tests__/%2.spec.tsx",
                        context = "test"
                    },
                    {
                        pattern = "(.*)/__tests__/(.*).spec.tsx$",
                        target = "%1/%2.tsx",
                        context = "source"
                    }
                }
            })
        end,
        keys = {
            {
                "<leader>TT",
                "<cmd>:Other<cr>",
                desc = "Switch to related file",
            },
            {
                "<leader>Tt",
                "<cmd>:Other test<cr>",
                desc = "Switch to test file",
            },
            {
                "<leader>Ts",
                "<cmd>:Other source<cr>",
                desc = "Switch to source file",
            },
        },
        lazy = true
    },

    -- buffer bar
    {
        'romgrk/barbar.nvim',
        dependencies = {
            'lewis6991/gitsigns.nvim',
            'nvim-tree/nvim-web-devicons',
        },
        init = function()
            vim.g.barbar_auto_setup = true
            vim.opt.sessionoptions:append 'globals'
            vim.api.nvim_create_user_command(
                'Mksession',
                function(attr)
                    vim.api.nvim_exec_autocmds('User', { pattern = 'SessionSavePre' })
                    vim.cmd.mksession { bang = attr.bang, args = attr.fargs }
                end,
                { bang = true, complete = 'file', desc = 'Save barbar with :mksession', nargs = '?' }
            )
        end,
        opts = {
            exclude_ft = { 'gitcommit' },
            auto_hide = 1,
            animation = false
        },
        keys = {
            {
                '<Leader>c',
                "<cmd>BufferClose<cr>",
                silent = true,
                desc = "Close buffer",
            },
            {
                '<Leader>k',
                "<cmd>BufferPick<cr>",
                silent = true,
                desc = "Pick buffer from buffer bar",
            },
            {
                '[w',
                "<cmd>BufferPrevious<cr>",
                silent = true,
                desc = "Previous buffer",
            },
            {
                ']w',
                "<cmd>BufferNext<cr>",
                silent = true,
                desc = "Next buffer",
            }
        },
        lazy = false
    },

    -- add gitlab support to fugitive
    "shumphrey/fugitive-gitlab.vim",

    -- split diff
    {
        "sindrets/diffview.nvim",
        opts = {
            DiffviewOpen = { "--imply-local" },
        }
    },

    -- rename ui
    {
        "smjonas/inc-rename.nvim",
        config = function()
            require("inc_rename").setup()

            vim.keymap.set("n", "<leader>rn", ":IncRename ", { desc = "Rename (clear input)" })
            vim.keymap.set("n", "<leader>rN", function()
                return ":IncRename " .. vim.fn.expand("<cword>")
            end, { expr = true, desc = "Rename (input with word)" })
        end,
    },

    -- auto formatting
    {
        'stevearc/conform.nvim',
        opts = {
            formatters_by_ft = {
                rust = { "rustfmt" },
                javascript = { "prettier" },
                json = { "prettier" },
                typescript = { "prettier" },
                sql = { "prettier" },
            },
            -- enable asynchronously formatting after saving
            format_after_save = {
                -- run the lsp formatter if other formatters are not found after save
                lsp_format = "fallback",
            },
        },
    },

    -- virtual text when debugging
    {
        'theHamsta/nvim-dap-virtual-text',
        opts = {}
    },

    -- git command integration
    "tpope/vim-fugitive",

    -- lets plugins utilize the repeat command
    "tpope/vim-repeat",

    -- auto detects file indentation
    "tpope/vim-sleuth",

    -- quickly replace surrounding characters like brackets and quotes with other surrounding characters
    "tpope/vim-surround",

    -- adds mappings for common forward, backward, and toggle actions, like switching buffers or toggling spell check
    "tpope/vim-unimpaired",

    -- lsp
    "VonHeikemen/lsp-zero.nvim",

    -- adds more text objects
    "wellle/targets.vim",

    -- package manager for lsps, debuggers, etc
    "williamboman/mason.nvim",

    -- automatic installation of lsp servers
    "williamboman/mason-lspconfig.nvim",

    -- autopair braces, etc
    {
        'windwp/nvim-autopairs',
        event = "InsertEnter",
        config = true
    },

    -- autoclose html tags
    {
        'windwp/nvim-ts-autotag',
        opts = {}
    },

    -- allows you to move highlighed text around
    {
        "zirrostig/vim-schlepp",
        config = function()
            vim.api.nvim_set_keymap("v", "<up>", "<Plug>SchleppUp", {})
            vim.api.nvim_set_keymap("v", "<down>", "<Plug>SchleppDown", {})
            vim.api.nvim_set_keymap("v", "<left>", "<Plug>SchleppLeft", {})
            vim.api.nvim_set_keymap("v", "<right>", "<Plug>SchleppRight", {})
            vim.api.nvim_set_keymap("v", "D", "<Plug>SchleppDup", {})
        end
    }
})

local lsp_zero = require('lsp-zero')

lsp_zero.on_attach(function(client, bufnr)
    -- see :help lsp-zero-keybindings to learn the available actions
    lsp_zero.default_keymaps({ buffer = bufnr })
end)

-- Set up lspconfig.
local capabilities = require('cmp_nvim_lsp').default_capabilities()
-- needed for nvim-ufo
capabilities.textDocument.foldingRange = {
    dynamicRegistration = false,
    lineFoldingOnly = true
}

-- Set up mason
require('mason').setup({})

-- set up dap adapters from mason
require("mason-nvim-dap").setup({
    ensure_installed = {
        "codelldb", -- used for rust debugging
    }
})

-- set up lsps from mason
local mason_lspconfig = require('mason-lspconfig')

mason_lspconfig.setup({
    ensure_installed = {
        'bashls',
        --'clangd',
        'cmake',
        'dockerls',
        'eslint',
        --'gopls',
        'html',
        'jsonls',
        'lua_ls',
        'pylyzer',
        'sqlls',
        'taplo',
        'ts_ls',
    },
    automatic_installation = true,
    handlers = {
        lsp_zero.default_setup,
    },
})

local lspconfig = require("lspconfig")

mason_lspconfig.setup_handlers {
    -- The first entry (without a key) will be the default handler
    -- and will be called for each installed server that doesn't have
    -- a dedicated handler.
    function(server_name) -- default handler
        lspconfig[server_name].setup {
            capabilities = capabilities,
        }
    end,

    ["jdtls"] = function()
        require('java').setup({
            jdk = {
                auto_install = false,
            },
            notifications = {
                dap = false,
            },
        })

        lspconfig.jdtls.setup({
            capabilities = capabilities,
            handlers = {
                -- By assigning an empty function, you can remove the notifications
                -- printed to the cmd
                ["$/progress"] = function(_, result, ctx) end,
            },
        })
    end,

    -- https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#lua_ls
    ["lua_ls"] = function()
        lspconfig.lua_ls.setup({
            capabilities = capabilities,
            on_init = function(client)
                local path = client.workspace_folders[1].name
                if vim.loop.fs_stat(path .. '/.luarc.json') or vim.loop.fs_stat(path .. '/.luarc.jsonc') then
                    return
                end

                client.config.settings.Lua = vim.tbl_deep_extend('force', client.config.settings.Lua, {
                    runtime = {
                        -- Tell the language server which version of Lua you're using
                        -- (most likely LuaJIT in the case of Neovim)
                        version = 'LuaJIT'
                    },
                    -- Make the server aware of Neovim runtime files
                    workspace = {
                        checkThirdParty = false,
                        library = {
                            vim.env.VIMRUNTIME
                            -- Depending on the usage, you might want to add additional paths here.
                            -- "${3rd}/luv/library"
                            -- "${3rd}/busted/library",
                        }
                        -- or pull in all of 'runtimepath'. NOTE: this is a lot slower
                        -- library = vim.api.nvim_get_runtime_file("", true)
                    }
                })
            end,
            settings = {
                Lua = {}
            }
        })
    end,

    -- https://github.com/mrcjkb/rustaceanvim/blob/master/doc/mason.txt#L36
    ['rust_analyzer'] = function() end,

    -- https://github.com/typescript-language-server/typescript-language-server/blob/master/docs/configuration.md
    ['ts_ls'] = function()
        lspconfig.ts_ls.setup {
            capabilities = capabilities,
            settings = {
                typescript = {
                    format = {
                        indentSize = 2
                    }
                }
            }
        }
    end
}

-- Makes j and k act like normal up and down on multi-lines and makes the <number> jump from multiple lines correct (for use with relative number liens)
vim.api.nvim_set_keymap("", "j", "(v:count == 0 ? 'gj' : 'j')", { noremap = true, silent = true, expr = true })
vim.api.nvim_set_keymap("", "k", "(v:count == 0 ? 'gk' : 'k')", { noremap = true, silent = true, expr = true })

-- adds af as a motion (yaf to copy the file, daf to delete the file, etc)
vim.api.nvim_set_keymap("o", "af", ":<C-u>normal! ggVG<CR>", { noremap = true })

-- make Y act like everything else
vim.api.nvim_set_keymap("n", "Y", "y$", {})

-- copy to clipboard
vim.keymap.set("v", "<leader>y", "\"*y", { desc = "Copy to clipboard" })
vim.keymap.set("n", "<leader>y", function()
    return '"*' .. vim.v.operator
end, { desc = "Copy to clipboard", expr = true })

-- add quick find and replace
vim.api.nvim_set_keymap("n", "<leader>S", ":%s//g<LEFT><LEFT>", { desc = "Quick find and replace" })

-- Dont put x into buffer
vim.api.nvim_set_keymap("n", "x", "\"_dl", {})

-- indent block of code
vim.api.nvim_set_keymap("n", "<leader>i", "vi{>", { desc = "Indent code block" })

-- move lines
vim.api.nvim_set_keymap("n", "<C-j>", "<cmd>:m .+1<CR>==", { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<C-k>", "<cmd>:m .-2<CR>==", { noremap = true, silent = true })
vim.api.nvim_set_keymap("i", "<C-j>", "<cmd>:m .+1<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("i", "<C-k>", "<cmd>:m .-2<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("v", "<C-j>", "<cmd>:m '>+1<CR>gv=gv", { noremap = true, silent = true })
vim.api.nvim_set_keymap("v", "<C-k>", "<cmd>:m '<-2<CR>gv=gv", { noremap = true, silent = true })

-- set tabs depending on file
vim.api.nvim_set_keymap("n", "<leader>te4", ":set expandtab tabstop=4 shiftwidth=4 softtabstop=4<CR>", {})
vim.api.nvim_set_keymap("n", "<leader>tn4", ":set noexpandtab tabstop=4 softtabstop=4 shiftwidth=4<CR>", {})
vim.api.nvim_set_keymap("n", "<leader>te8", ":set expandtab tabstop=8 softtabstop=8 shiftwidth=8<CR>", {})
vim.api.nvim_set_keymap("n", "<leader>tn8", ":set noexpandtab tabstop=8 softtabstop=8 shiftwidth=8<CR>", {})
vim.api.nvim_set_keymap("n", "<leader>te2", ":set expandtab tabstop=2 shiftwidth=2 softtabstop=2<CR>", {})
vim.api.nvim_set_keymap("n", "<leader>tn2", ":set noexpandtab tabstop=2 shiftwidth=2 softtabstop=2<CR>", {})

-- keep cursor in place when joining lines
vim.api.nvim_set_keymap("n", "J", "mzJ`z", { noremap = true })

-- show diagnostics
vim.keymap.set("n", "<leader>d", function() vim.diagnostic.open_float() end,
    { silent = true, desc = "Show diagnostics" })

-- templates
vim.api.nvim_set_keymap("n", "<leader>tl1", ":-1read ~/.vim/templates/lorem1.txt<cr>", { desc = "Lorem 1" })
vim.api.nvim_set_keymap("n", "<leader>tl2", ":-1read ~/.vim/templates/lorem2.txt<cr>", { desc = "Lorem 2" })
-- templates javascript
vim.api.nvim_set_keymap("n", "<leader>tjc", "iconsole.log(\"\");<esc>2hi", {})

vim.cmd([[filetype plugin indent on]])

-- theme settings
vim.cmd.colorscheme "vscode"

-- paste from clipboard properly (even "set paste" stopped working properly...?)
vim.api.nvim_set_keymap("n", "<leader>p", '"+p', { silent = true })

-- write to file quickly
vim.api.nvim_set_keymap("n", "<Leader>w", ":w<CR>", { noremap = true, silent = true, desc = "Save file" })
vim.api.nvim_set_keymap("n", "<Leader>q", ":q<CR>", { noremap = true, silent = true, desc = "Quit window" })
vim.api.nvim_set_keymap("n", "<Leader>Q", ":qa<CR>", { noremap = true, silent = true, desc = "Quit all windows" })

-- search with /find then \Yreplace<cr> to replace all matched items in the search
vim.api.nvim_set_keymap("n", "Q", ":%s/' . @/ . '//g<LEFT><LEFT>", { expr = true })

-- Sudo to write
vim.api.nvim_set_keymap("n", "<leader>:w", ":w !sudo tee % >/dev/null", { desc = "Save with sudo" })

-- make arrow keys more useful, resize viewports
vim.api.nvim_set_keymap("n", "<Left>", ":vertical resize +2<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<Right>", ":vertical resize -2<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<Up>", ":resize -2<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<Down>", ":resize +2<CR>", { noremap = true, silent = true })

-- Custom commands with <leader>
-- Edit vimrc
vim.api.nvim_set_keymap("n", "<leader>e", ":sp $MYVIMRC<cr>", { desc = "Edit settings" })

-- Terminal
vim.api.nvim_set_keymap("t", "<Esc>", "<C-><C-n>", { noremap = true })
vim.api.nvim_set_keymap("t", "<A-h>", "<C-><C-n><C-w>h", { noremap = true })
vim.api.nvim_set_keymap("t", "<A-j>", "<C-><C-n><C-w>j", { noremap = true })
vim.api.nvim_set_keymap("t", "<A-k>", "<C-><C-n><C-w>k", { noremap = true })
vim.api.nvim_set_keymap("t", "<A-l>", "<C-><C-n><C-w>l", { noremap = true })
vim.api.nvim_set_keymap("n", "<A-h>", "<C-w>h", { noremap = true })
vim.api.nvim_set_keymap("n", "<A-j>", "<C-w>j", { noremap = true })
vim.api.nvim_set_keymap("n", "<A-k>", "<C-w>k", { noremap = true })
vim.api.nvim_set_keymap("n", "<A-l>", "<C-w>l", { noremap = true })

-- dap
local dap, dapui = require("dap"), require("dapui")
dap.listeners.before.attach.dapui_config = function()
    dapui.open()
end
dap.listeners.before.launch.dapui_config = function()
    dapui.open()
end

-- See https://github.com/mfussenegger/nvim-dap/wiki/Debug-Adapter-installation#vscode-js-debug
dap.adapters["pwa-node"] = {
    type = "server",
    host = "localhost",
    port = "${port}",
    executable = {
        command = "node",
        -- 💀 Make sure to update this path to point to your installation
        args = { os.getenv('HOME') .. "/.vim/js-debug/src/dapDebugServer.js", "${port}" },
    }
}
dap.configurations.javascript = {
    {
        type = "pwa-node",
        request = "launch",
        name = "Launch file",
        program = "${file}",
        cwd = "${workspaceFolder}",
    },
}

-- jump to errors first for diagnostics
-- https://github.com/neovim/neovim/discussions/25588#discussioncomment-8700283
-- I'd prefer if it stayed on the error if it exists, but this is good enough
local function pos_equal(p1, p2)
    local r1, c1 = unpack(p1)
    local r2, c2 = unpack(p2)
    return r1 == r2 and c1 == c2
end

local function goto_error_then_hint()
    local pos = vim.api.nvim_win_get_cursor(0)
    vim.diagnostic.goto_next({ severity = vim.diagnostic.severity.ERROR, wrap = true })
    local pos2 = vim.api.nvim_win_get_cursor(0)
    if (pos_equal(pos, pos2)) then
        vim.diagnostic.goto_next({ wrap = true })
    end
end

local function goto_error_then_hint_prev()
    local pos = vim.api.nvim_win_get_cursor(0)
    vim.diagnostic.goto_prev({ severity = vim.diagnostic.severity.ERROR, wrap = true })
    local pos2 = vim.api.nvim_win_get_cursor(0)
    if (pos_equal(pos, pos2)) then
        vim.diagnostic.goto_prev({ wrap = true })
    end
end

vim.keymap.set("n", "]d", goto_error_then_hint, { desc = "Next diagnostic" })
vim.keymap.set("n", "[d", goto_error_then_hint_prev, { desc = "Previous diagnostic" })

-- run tests in buffer since neotest-java and nvim-java don't work for me
local function split(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t = {}
    for str in string.gmatch(inputstr, "([^" .. sep .. "]+)") do
        table.insert(t, str)
    end
    return t
end

local buffer = nil
local function run_with_output(cmd)
    if buffer == nil then
        local win = vim.api.nvim_get_current_win()
        buffer = vim.api.nvim_create_buf(true, true)
        vim.api.nvim_open_win(buffer, false, { split = 'below', win = win })
        vim.api.nvim_buf_call(buffer, function() vim.cmd(':set nonu') end)

        vim.api.nvim_create_autocmd("BufUnload", {
            buffer = buffer,
            callback = function()
                buffer = nil
            end
        })
    end

    vim.api.nvim_buf_set_lines(buffer, -1, -1, true,
        { "--------------------------------------------------------------------------------" })
    vim.api.nvim_buf_set_lines(buffer, -1, -1, true, { table.concat(cmd, " ") })
    vim.api.nvim_buf_call(buffer, function() vim.cmd.normal('G') end)

    local on_read = function(err, data)
        if err ~= nil then
            print(err)
        end
        if data ~= nil then
            vim.schedule(function()
                vim.api.nvim_buf_set_lines(buffer, -1, -1, true, split(data, '\n'))
                vim.api.nvim_buf_call(buffer, function() vim.cmd.normal('G') end)
            end)
        end
    end

    vim.system(cmd, { text = true, stderr = on_read, stdout = on_read })
end

local last_command = nil

local function test_file()
    vim.cmd.update()
    last_command = { './gradlew', 'test', '-i', '--tests', vim.fn.expand('%:t:r') }
    run_with_output(last_command)
end

local function test_one()
    vim.cmd.update()
    local win = vim.api.nvim_get_current_win()
    local current_pos = vim.api.nvim_win_get_cursor(win)
    vim.fn.search('@\\(Parameterized\\)\\?Test', 'b')
    vim.fn.search('void ')
    vim.cmd.normal('W')
    local test_name = vim.fn.expand('<cword>')
    vim.api.nvim_win_set_cursor(win, current_pos)
    last_command = { './gradlew', 'test', '-i', '--tests', vim.fn.expand('%:t:r') .. '.' .. test_name }
    run_with_output(last_command)
end

local function test_last()
    run_with_output(last_command)
end

vim.keymap.set('n', '<leader>jt', test_one, { desc = "Run test (java)" })
vim.keymap.set('n', '<leader>jf', test_file, { desc = "Run test file (java)" })
vim.keymap.set('n', '<leader>jj', test_last, { desc = "Rerun test (java)" })
